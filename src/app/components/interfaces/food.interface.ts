export interface IFood {
  title: string;
  photoUrl: string;
  category: string;
}