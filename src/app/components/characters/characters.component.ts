import { Component, OnInit } from '@angular/core';
import { ICharacters} from '../interfaces/characters.interface';
import { apiResponse } from '../api/characters.api';


@Component({
  selector: 'app-characters',
  templateUrl: './characters.component.html',
  styleUrls: ['./characters.component.scss']
})
export class CharactersComponent implements OnInit {

  constructor() { }
      public politics: ICharacters[];
  ngOnInit() {
     this.politics =  apiResponse;
  }

}
