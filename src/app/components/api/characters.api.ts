export const apiResponse = [
  {
    name: 'Pedro Sanchez',
    // tslint:disable-next-line: max-line-length
    photoUrl: 'https://upload.wikimedia.org/wikipedia/commons/thumb/2/24/Pedro_S%C3%A1nchez_in_2018d.jpg/440px-Pedro_S%C3%A1nchez_in_2018d.jpg',
    politicParty: 'PSOE',
  },
  {
    name: 'Pablo Casado',
    // tslint:disable-next-line: max-line-length
    photoUrl: 'https://upload.wikimedia.org/wikipedia/commons/thumb/e/e9/Pablo_Casado_2019d_%28cropped%29.jpg/440px-Pablo_Casado_2019d_%28cropped%29.jpg',
    politicParty: 'PP',
  },
];
