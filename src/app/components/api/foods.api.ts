export const apiResponse = [
  {
    title: 'Spagetti',
    // tslint:disable-next-line: max-line-length
    photoUrl: 'https://cdn6.recetasdeescandalo.com/wp-content/uploads/2019/05/Spaguetti-a-la-bolonesa-una-receta-de-pasta-para-triunfar.jpg',
    category: 'pasta',
  },
  {
    title: 'ensalada de apio, tomate y aguacate',
    // tslint:disable-next-line: max-line-length
    photoUrl: 'https://t1.rg.ltmcdn.com/es/images/1/7/7/ensalada_de_apio_tomate_y_aguacate_60771_600.jpg',
    category: 'vegetariano',
  },
  {
    title: 'paella',
    // tslint:disable-next-line: max-line-length
    photoUrl: 'https://cdn5.recetasdeescandalo.com/wp-content/uploads/2019/09/Paella-mixta-con-marisco-carne-y-verduras.-Receta-de-arroz-deliciosa.jpg',
    category: 'mediterranea',
  },
];